Aspect Based Aspect Extraction model in Pytorch based on [this repository](https://github.com/ruidan/Unsupervised-Aspect-Extraction) for English language.
Dataset can be downloaded from the original repository.
